/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    textButton1.setButtonText("Click Me");
    addAndMakeVisible(textButton1);
    textButton1.addListener(this);
    
    textButton2.setButtonText("No, Click Me!");
    addAndMakeVisible(textButton2);
    textButton2.addListener(this);
    
    slider1.setSliderStyle(Slider::Rotary);
    addAndMakeVisible(slider1);
    slider1.addListener(this);
    
    comboBox1.addItem("Item 1", 1);
    comboBox1.addItem("Item 2", 2);
    comboBox1.addItem("Item 3", 3);
    addAndMakeVisible(comboBox1);
    
    textEditor1.setText ("Add some text here");
    addAndMakeVisible(textEditor1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG ("Print this variable: " << getWidth() << " and this variable: " << getHeight());
    textButton1.setBounds (10, 10, getWidth() - 20, 40);
    slider1.setBounds (10, 50, getWidth() - 20, 40);
    comboBox1.setBounds (10, 90, getWidth()-20, 40);
    textEditor1.setBounds (10, 130, getWidth()-20, 40);
    textButton2.setBounds(10, 170, getWidth() -20, 40);
    
}
void MainComponent::buttonClicked(Button* button)
{
    if (button == &textButton1)
        DBG("Button 1 Clicked\n");
    else if (button == &textButton2)
        DBG("Button 2 Clicked\n");
}
void MainComponent::sliderValueChanged(Slider* slider)
{
    DBG("Slider value is" << slider1.getValue() <<"\n");
}
void MainComponent::paint (Graphics& g)
{
    int halfWidth = (getWidth()/2.0);
    int halfHeight = (getHeight()/2.0);
    
    DBG("MSPaint.exe");
    g.setColour(Colours::hotpink);
    g.fillRoundedRectangle(halfWidth, halfHeight, halfWidth, halfHeight, 20);
}
